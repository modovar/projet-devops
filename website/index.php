
<html>
    <head>
        <title>POZOS</title>
    </head>

    <body>
        <h1>Projet DevOps</h1>
        <ul>
            <form action="" method="POST">
            <button type="submit" name="submit">Liste des Etudiants</button>
            </form>

            <?php
              if($_SERVER['REQUEST_METHOD'] == "POST" and isset($_POST['submit']))
              {
              $username = getenv('USERNAME');
              $password = getenv('PASSWORD');
              if ( empty($username) ) $username = 'fake_username';
              if ( empty($password) ) $password = 'fake_password';
              $context = stream_context_create(array(
                "http" => array(
                "header" => "Authorization: Basic " . base64_encode("$username:$password"),
              )));

              $url = 'http://api-pozos:5000/pozos/api/v1.0/get_student_ages';
              $list = json_decode(file_get_contents($url, false, $context), true);
              echo "<p style='color:blue;; font-size: 20px;'>LA liste/p>";
              foreach($list["student_ages"] as $key => $value) {
                  echo "- $key are $value years old <br>";
              }
             }
            ?>
        </ul>
    </body>
</html>
